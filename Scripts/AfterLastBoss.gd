extends KinematicBody2D

var velocity = Vector2()
var move_speed = 100
var moveState = ""

func setup(level):
	pass

func _ready():
	move("down")
	yield(get_tree().create_timer(2), "timeout")
	move("stay")

func _process(delta):
	move(moveState)

# BASIC MOVEMENT
func move(move_state):
	if move_state == "down":
		velocity = Vector2(0,1)
	if move_state == "up":
		velocity = Vector2(0,-1)
	if move_state == "left":
		velocity = Vector2(-1,0)
	if move_state == "right":
		velocity = Vector2(1,0)
	if move_state == "stay":
		velocity = Vector2(0,0)
	move_and_slide(velocity * move_speed)
