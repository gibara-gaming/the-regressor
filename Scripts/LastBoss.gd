extends KinematicBody2D

var velocity = Vector2()
var move_speed = 100
var moveState = ""
var canDamage = false

# Sprinkle setup
var aimLeft = self.rotation_degrees + (PI/3)
var aimRight = self.rotation_degrees - (PI/3)
var sprinkleShootCounter = 0
var sprinkleState = "normal"

# Normal shoot setup
var normalShootCounter = 0
var normalShootCooldown = 0

export var live = 1250 setget set_live 
export (PackedScene) var Bullet_enemy
export (PackedScene) var Destroyed
signal hit
signal boss_dead

func setup(level):
	pass

func _ready():
	pass

func _process(delta):
	pass
	
func _physics_process(delta):
	if live == 50:
		self.move_speed += 50
	move(moveState)

func normalShoot_start():
	$NormalShootRate.start()
	$NormalShootRate.start()

func normalShoot_stop():
	$NormalShootRate.stop()
	$NormalShootRate.stop()

func normalShoot():
	var middleCannon = $Cannons/middle.get_global_position()
	var leftCannon1 = $Cannons/left1.get_global_position()
	var leftCannon2 = $Cannons/left2.get_global_position()
	var rightCannon1 = $Cannons/right1.get_global_position()
	var rightCannon2 = $Cannons/right2.get_global_position()
	var aim = self.rotation_degrees
	if normalShootCounter <= 5:
		create_bullet(middleCannon, aim)
		create_bullet(leftCannon2, aim + (PI/4))
		create_bullet(leftCannon1, aim+(PI/3))
		create_bullet(rightCannon2, aim - (PI/4))
		create_bullet(rightCannon1, aim-(PI/3))
		normalShootCounter += 1
		print(normalShootCounter)

func sprinkleShotStart():
	$SprinkleFireRate.start()

func sprinkleShotStop():
	$SprinkleFireRate.stop()

func sprinkleShot():
	var leftCannon3 = $Cannons/left3.get_global_position()
	var rightCannon3 = $Cannons/right3.get_global_position()
	sprinkleShootCounter += 1
	if sprinkleShootCounter == 10 && sprinkleState == "normal":
		sprinkleShootCounter = 0
		sprinkleState = "reverse"
	if sprinkleShootCounter == 10 && sprinkleState == "reverse":
		sprinkleShootCounter = 0
		sprinkleState = "normal"
	sprinkleControl(sprinkleState)
	create_bullet(leftCannon3, aimLeft)
	create_bullet(rightCannon3, aimRight)

func sprinkleControl(state):
	if state == "normal":
		aimLeft -= PI/10
		aimRight += PI/10
	if state == "reverse":
		aimLeft += PI/10
		aimRight -= PI/10

# BASIC MOVEMENT
func move(move_state):
	if move_state == "down":
		velocity = Vector2(0,1)
	if move_state == "up":
		velocity = Vector2(0,-1)
	if move_state == "left":
		velocity = Vector2(-1,0)
	if move_state == "right":
		velocity = Vector2(1,0)
	if move_state == "stay":
		velocity = Vector2(0,0)
	move_and_slide(velocity * move_speed)
	
func stay():
	velocity = Vector2(0,0)
#	move_and_slide(velocity * move_speed)

func set_position(pos):
	self.position.x = pos[0]
	self.position.y = pos[1]

func set_live(value):
	if canDamage:
		live -= value
	if live <= 0:
		get_parent().get_parent().get_parent().get_parent()._last_boss_die()
		emit_signal("last_boss_dead")
		destroyed_anim() 
		get_parent().get_parent().get_parent().queue_free()
		
func destroyed_anim():
	var ex = Destroyed.instance()
	ex.position = get_global_position()
	get_tree().get_root().add_child(ex)
	
func create_bullet(pos, direction):
	var bullet = Bullet_enemy.instance()
	bullet.position = pos
	bullet.rotation = direction
	bullet.velocity = bullet.velocity.rotated(direction)
	get_tree().get_root().add_child(bullet)

func _on_Area2D_area_entered(area):
	if area.is_in_group("player"):
		area.armor -= 1

func _on_NormalShootRate_timeout():
	$NormalShootRate.stop()
	if normalShootCounter >= 5:
		yield(get_tree().create_timer(normalShootCooldown), "timeout")
		normalShootCounter = 0
		normalShoot()
	else:
		normalShoot()
	$NormalShootRate.start()

func _on_SprinkleFireRate_timeout():
	sprinkleShot()

