extends Path2D

onready var follow = get_node("PathFollowP3")
onready var moveSpeed = 250

var nodeName = "Phase3Path"
var timer = 0

func _ready():
	set_process(false)

func _process(delta):
	var boss = self.get_parent().boss
	
	if (boss && boss.state == "phase3"):
		timer += 1
		follow.set_offset(follow.get_offset() - moveSpeed * delta)

		if timer > 230:
			boss.state = "entry"
#			self.get_parent().init_phase4()
			timer = 0
