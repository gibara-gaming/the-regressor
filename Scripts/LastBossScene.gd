extends Node2D

onready var lastBoss = get_node("LastBoss")
onready var maxHealth = lastBoss.live
onready var pathFollow = get_node("LastBossPath/PathFollow2D")
onready var bossPath = get_node("LastBossPath")
onready var healthBarScene = get_node("LastBossHealthBar")
onready var healthBar = get_node("LastBossHealthBar/ProgressBar")

var num = 0

func _ready():
	self.set_process(false)
	lastBoss.moveState = "down"
	yield(get_tree().create_timer(4),"timeout")
	lastBoss.moveState = "stay"
	yield(get_tree().create_timer(3),"timeout")

func _process(delta):
	phaseControl()
	
func _physics_process(delta):
	pass

func phaseControl():
	var currentHealth = get_node("LastBossPath/PathFollow2D/LastBoss").live
	if currentHealth <= maxHealth/2:
		lastBoss.move_speed = 200
		lastBoss.get_node("NormalShootRate").wait_time = 0.1
		lastBoss.normalShootCooldown = 0
		bossPath.moveSpeed = lastBoss.move_speed
		

func pattern1():
	pathFollow.loop = true
	lastBoss.sprinkleShotStop()
	lastBoss.normalShoot_start()

func pattern2():
	pathFollow.loop = false
	yield(get_tree().create_timer(1),"timeout")
	lastBoss.sprinkleShotStart()

func startBattle():
	self.remove_child(lastBoss)
	pathFollow.add_child(lastBoss)
	# Update after reparent
	self.set_process(true) 
	lastBoss = get_node("LastBossPath/PathFollow2D/LastBoss")
	lastBoss.get_node("CollisionShape2D").disabled = false
	lastBoss.canDamage = true
	bossPath.set_process(true)
	# Healthbar setup
	healthBar.max_value = lastBoss.live
	healthBarScene.visible = true
	healthBarScene.set_process(true)
	patternLoop()

func patternLoop():
	while true:
		pattern1()
		yield(get_tree().create_timer(10),"timeout")
		pattern2()
		yield(get_tree().create_timer(10),"timeout")
