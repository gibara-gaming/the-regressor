extends Node2D

onready var boss = get_node("Boss")
onready var health_bar = get_node("HealthBossBarControl")

var p3 = null
var p4 = null
var state = null


func _ready():
	init_phase3()


func init_phase3():
	self.remove_child(boss)
	
	var boss_path = get_node("Phase3Path")
	p3 = get_node("Phase3Path/PathFollowP3")
	p3.add_child(boss)
	
	boss = get_node("Phase3Path/PathFollowP3/Boss")
	boss.get_node("CollisionShape2D").disabled = false
	
	boss_path.set_process(true)
#	p3.loop = true


func init_phase4():
	self.get_node("Phase3Path").set_process(false)
	
	boss = get_node("Phase3Path/PathFollowP3/Boss")
	self.get_node("Phase3Path/PathFollowP3").remove_child(boss)
	
	var boss_path = get_node("Phase4Path")
	p4 = get_node("Phase4Path/PathFollowP4")
	p4.add_child(boss)
	
	boss = get_node("Phase4Path/PathFollowP4/Boss")
	boss.get_node("CollisionShape2D").disabled = false
	
	boss_path.set_process(true)
	p4.loop = true


func end_phase():
	self.get_node("Phase4Path").set_process(false)

	boss = get_node("Phase4Path/PathFollowP4/Boss")
	self.get_node("Phase4Path/PathFollowP4").remove_child(boss)
	
	self.add_child(boss)
	boss = get_node("Boss")
	boss.state = "entry"

	init_phase3()

