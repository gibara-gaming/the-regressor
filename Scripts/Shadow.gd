extends Area2D


export (PackedScene) var Bullet
export (PackedScene) var Destroyed
export var base_speed = 400
var speed = base_speed
var screen_size
export var armor = 5 setget set_armor
signal hit
signal dead

onready var player = get_node("../Ship")
var shadow_movement_index = 0;
var start = false

#fire rate up, spread, shield
var powerup = [false, false, false]

var hitable = true

var dim_count = 0
var max_dim_count = 10

var ghost_mode = false

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide()


func setup(level):
	$ShootTimer.wait_time = float(1/level)


func _process(delta):
	pass

func set_armor(value):
	if !hitable:
		return

	if powerup[2]:
		return

	create_explosion()
	armor = value
	emit_signal("hit")
	if armor <= 0:
		emit_signal("dead")
		$ShootTimer.stop()
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
		queue_free()

	if hitable && armor > 0 && !powerup[2]:
		activate_ghost_mode()


func start(pos, player_armor):
	deactivate_fire_rate_up()
	deactivate_spread()
	deactivate_shield()
	deactivate_ghost_mode()

	armor = player_armor
	
	$ShootTimer.wait_time = 0.25
	powerup = [false, false, false]

	position = pos
	show()
	$ShootTimer.start()
	$CollisionShape2D.disabled = false


func create_bullet(pos, velocity_vector, image = null):
	var bullet = Bullet.instance()
	bullet.position = pos
	bullet.velocity = velocity_vector
	if image:
		bullet.get_child(0).set_texture(image)
	get_tree().get_root().add_child(bullet)


func shoot():
	
	#Spread
	if (powerup[1]):
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(100, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(25, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(50, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(75, -500))
		create_bullet($Cannons/FrontCenter.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-100, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-75, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-50, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-25, -500))
	else:
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontCenter.get_global_position(), Vector2(0, -500))

func activate_power_up(powerup_number):
	powerup[powerup_number] = true

	if powerup_number == 0:
		activate_fire_rate_up()
	elif powerup_number == 1:
		activate_spread()
	elif powerup_number == 2:
		activate_shield()

func activate_fire_rate_up():
	$ShootTimer.wait_time = 0.1
	$FireRateUpDuration.stop()
	$FireRateUpDuration.start()
	
func deactivate_fire_rate_up():
	powerup[0] = false	
	$ShootTimer.wait_time = 0.25

func activate_spread():
	powerup[1] = true
	$SpreadDuration.stop()
	$SpreadDuration.start()

func deactivate_spread():
	powerup[1] = false
	
func activate_shield():
	powerup[2] = true
	hitable = false
	$Shield.visible = true
	$ShieldDuration.stop()
	$ShieldDuration.start()
	
func deactivate_shield():
	hitable = true
	powerup[2] = false
	$Shield.visible = false

#On Hit Something
func activate_ghost_mode():
	ghost_mode = true
	hitable = false
	if (dim_count % 2 == 0):
		$character.modulate = Color(255, 255, 255, 255)
	else:
		$character.modulate = Color(0.913725,0.996078,0.615686,1)
	$GhostModeDuration.stop()
	$GhostModeDuration.start()
	   
func deactivate_ghost_mode():
	hitable = true
	ghost_mode = false
	$character.modulate = Color(0.913725,0.996078,0.615686,1)
	$GhostModeDuration.stop()

func create_explosion():
	var explosion = Destroyed.instance()
	explosion.position = get_global_position()
	get_tree().get_root().add_child(explosion)


func _on_ShootTimer_timeout():
	if Global.dialog_on:
		return
	shoot()

func _on_SpreadDuration_timeout():
	deactivate_spread()
	emit_signal("powerup_change")

func _on_FireRateUpDuration_timeout():
	deactivate_fire_rate_up()
	emit_signal("powerup_change")
	
func _on_ShieldDuration_timeout():
	deactivate_shield()
	emit_signal("powerup_change")

func _on_GhostModeDuration_timeout():
	dim_count += 1
	if dim_count < max_dim_count:
		activate_ghost_mode()
	else:
		dim_count = 0
		deactivate_ghost_mode()
