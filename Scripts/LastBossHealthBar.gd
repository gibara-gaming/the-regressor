extends Node2D
onready var healthBar = get_node("ProgressBar")

func _ready():
	set_process(false)
	self.position.x = 217.255
	self.position.y = 50.942

func _process(delta):
	healthBarUpdate()


func healthBarUpdate():
	var currentHP = get_parent().get_node("LastBossPath/PathFollow2D/LastBoss").live
	healthBar.value = currentHP
