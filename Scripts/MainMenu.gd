extends ColorRect

signal to_start_game
signal to_guide

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_NewGame_pressed():
	emit_signal("to_start_game") # Replace with function body.


func _on_Guide_pressed():
	emit_signal("to_guide")
