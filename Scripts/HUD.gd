extends CanvasLayer
signal start_game
signal next_iteration
signal wave_3_start
signal second_dialog
signal next_level
signal quit_play
signal last_boss_start
signal end_part_start

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var powerup_1_image = preload("res://Assets/powerup/powerup_1.png")
var powerup_2_image = preload("res://Assets/powerup/powerup_2.png")
var powerup_3_image = preload("res://Assets/powerup/powerup_3.png")

var powerup_1_image_greyscale = preload("res://Assets/powerup/powerup_1_greyscale.png")
var powerup_2_image_greyscale = preload("res://Assets/powerup/powerup_2_greyscale.png")
var powerup_3_image_greyscale = preload("res://Assets/powerup/powerup_3_greyscale.png")

var dialog_box = null

# Called when the node enters the scene tree for the first time.
func _ready():
	$NextButton.hide()
	pass # Replace with function body.

func start(armor, skill, level):
	update_nuke(armor)
	update_skill(skill)

func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()


func show_game_over():
	$PauseButton.hide()
	show_message("Game Over")
	# Wait until the MessageTimer has counted down.
	yield($MessageTimer, "timeout")
	
	$Message.text = "Next Iteration"
	$Message.show()
	# Make a one-shot timer and wait for it to finish.
	yield(get_tree().create_timer(1.5), "timeout")
	#$StartButton.show()
	emit_signal("next_iteration")


func show_next_level():
	show_message("Next Level")
	yield($MessageTimer, "timeout")
	
	$Message.text = "Stay Alive!"
	$Message.show()
	yield(get_tree().create_timer(1), "timeout")
	$NextButton.show()


func update_nuke(armor):
	$NukeLabel.text = str("Lives: ", (armor))


func update_iteration(iteration):
	$LevelLabel.text = str("Iteration: ", iteration)

func update_skill(skill_count):
	$UltimateLabel.text = str("Ultimate: ", skill_count)
	
func update_kill_score(kill_score):
	$KillScoreLabel.text = str("Score: ", kill_score)

func _on_StartButton_pressed():
	$StartButton.hide()
	emit_signal("start_game")

func _on_MessageTimer_timeout():
	$Message.hide()

func _on_NextButton_pressed():
	$NextButton.hide()
	emit_signal("next_level")

func update_powerup(powerup):
	if powerup[0]:
		$FireRateUp.set_texture(powerup_1_image)
		$FireRURemainingTime.show()
	else:
		$FireRateUp.set_texture(powerup_1_image_greyscale)
		$FireRURemainingTime.hide()
		
	if powerup[1]:
		$Spread.set_texture(powerup_2_image)
		$SpreadRemainingTime.show()
	else:
		$Spread.set_texture(powerup_2_image_greyscale)
		$SpreadRemainingTime.hide()
		
	if powerup[2]:
		$Shield.set_texture(powerup_3_image)
		$ShieldRemainingTime.show()
	else:
		$Shield.set_texture(powerup_3_image_greyscale)
		$ShieldRemainingTime.hide()

func update_powerup_duration(powerup, time_left):
	if powerup == "firerateup":
		$FireRURemainingTime.text = time_left
	
	if powerup == "spread":
		$SpreadRemainingTime.text = time_left
	
	if powerup == "shield":
		$ShieldRemainingTime.text = time_left


func start_new_game():
	$MainMenu.hide()
	Global.shadows = []
	Global.shadows_movement = []
	emit_signal("start_game")

func show_guide():
	$Guide.show()
	$MainMenu.hide()

func back_to_main_menu():
	$MainMenu.show()
	$Guide.hide()

func _on_PauseButton_pressed():
	get_tree().paused = true
	$Pause.show()

func hide_pause_menu():
	$Pause.hide()
	get_tree().paused = false

func quit_confirmed():
	$Pause.hide()
	get_tree().paused = false
	$PauseButton.hide()
	emit_signal("quit_play")
	$MainMenu.show()

func run_dialog(current_dialog, part_number):
	if (!dialog_box):
		dialog_box = $DialogBox/DialogText
	dialog_box.run_dialog(current_dialog, part_number)


func _on_YesButton_pressed():
	get_node('Pause').emit_signal("quit_confirmed")
	get_node('WinScreen').visible =  false
