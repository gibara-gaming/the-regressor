extends Path2D

onready var follow = get_node("PathFollow2D")
var nodeName = "LastBossPath"
onready var moveSpeed = get_parent().get_node("LastBoss").move_speed

func _ready():
	set_process(false)

func _process(delta):
	follow.set_offset(follow.get_offset() - moveSpeed * delta)
