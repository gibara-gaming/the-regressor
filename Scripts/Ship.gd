extends Area2D


export (PackedScene) var Bullet
export (PackedScene) var Destroyed
export (PackedScene) var Explosion

export var base_speed = 400
var speed = base_speed
var screen_size
var in_skill = false

export var armor = 5 setget set_armor
export var skill = 2 setget set_skill

signal hit
signal dead
signal skill

signal powerup_change
signal powerup_duration_change

onready var player = get_node("../Ship")
var shadow_movement_index = 0;
var start = false;
var x_min = 0;
var x_max = 0;
var y_min = 0;
var y_max = 0;

#fire rate up, spread, shield
var powerup = [false, false, false]

var hitable = true

var dim_count = 0
var max_dim_count = 10

var ghost_mode = false

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	hide()

func setup():
	pass

func set_play_area(area_x_min, area_x_max, area_y_min, area_y_max):
	x_min = area_x_min;
	x_max = area_x_max;
	y_min = area_y_min;
	y_max = area_y_max;

func ult():
	timeSlowAnimation()
	in_skill = true
	Engine.time_scale = 0.5
	$ShootTimer.wait_time = float($ShootTimer.wait_time) / 2
	base_speed = base_speed * 2
	yield(get_tree().create_timer(5), "timeout")	# Nyesuain sama node UltimateDuration
	Engine.time_scale = 1
	$ShootTimer.wait_time = float($ShootTimer.wait_time) * 2
	base_speed = base_speed / 2
	in_skill = false
	

func _process(delta):
	if Global.dialog_on:
		return
	
	if (!start):
		return;

	var velocity = Vector2()
	if Input.is_action_pressed("slow"):
		speed = base_speed/2
	else:
		speed = base_speed
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	
	if skill > 0 && Input.is_action_just_pressed("skill") && !in_skill:
		skill = skill - 1
		emit_signal("skill")
		ult()
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed

	position += velocity * delta
	position.x = clamp(position.x, x_min, x_max)
	position.y = clamp(position.y, y_min, y_max)
	
	if start:
		#Move shadow
		for i in range(Global.shadows.size()):
			var shadow = Global.shadows[i];
			var movement = Global.shadows_movement[i]
			if ( shadow_movement_index < movement.size()-1 && shadow):
				shadow.position = movement[shadow_movement_index];
				shadow.visible = true;
			else:
				if (shadow):
					get_node("../../Stage").remove_child(shadow)
		
		shadow_movement_index += 1;
		
		#Init shadow
		Global.shadows_movement[Global.shadows_movement.size() - 1].append(player.get_global_position())

# time slow animation and sound
func timeSlowAnimation():
	$startUltimateGroup.visible = true
	get_node("startUltimateGroup/WorldEnvironment").get_environment().glow_enabled = true
	get_node("startUltimateGroup/startUltimate").playing = true
	$startUltimateSound.play()
	$UltimateDuration.start()
	$ultimate.visible = true

func _on_UltimateDuration_timeout():
	$endUltimate.visible = true
	$endUltimate.playing = true
	$ultimate.visible = false
	$endUltimateSound.play()
	$UltimateDuration.stop()

func _on_endUltimate_animation_finished():
	get_node("startUltimateGroup/WorldEnvironment").get_environment().glow_enabled = false
	$endUltimate.visible = false
	$endUltimate.frame = 0
	$endUltimate.playing = false

func _on_startUltimate_animation_finished():
	var node = get_node("startUltimateGroup/startUltimate")
	node.frame = 0
	node.playing = false
	$startUltimateGroup.visible = false

#HERE
func set_armor(value):
	if !hitable:
		return
	
	if powerup[2]:
		return

#	create_explosion()
	armor = value
	emit_signal("hit")
	
	if armor <= 0:
		create_explosion_dead()
		emit_signal("dead")
		$ShootTimer.stop()
		hide()
		$CollisionShape2D.set_deferred("disabled", true)
		#queue_free()
		
	if hitable && armor > 0 && !powerup[2]:
		activate_ghost_mode()

func set_skill(value):
	skill = value

func start(pos):
	deactivate_fire_rate_up()
	deactivate_spread()
	deactivate_shield()
	deactivate_ghost_mode()
	
	armor = 5
	skill = 2

	$ShootTimer.wait_time = 0.25
	
	powerup = [false, false, false]
	emit_signal("powerup_change")

	shadow_movement_index = 0;
	Global.shadows_movement.append([]);
	Global.shadows = [];
	for movement in Global.shadows_movement:
		var shadow = Global.shadowScene.instance();
		Global.shadows.append(shadow);
		get_node("../../Stage").add_child(shadow)
		shadow.start(pos, armor)
	position = pos
	start = true;
	show()
	$ShootTimer.start()
	$CollisionShape2D.disabled = false


func create_bullet(pos, velocity_vector, image = null):
	var bullet = Bullet.instance()
	bullet.position = pos
	bullet.velocity = velocity_vector
	if image:
		bullet.get_child(0).set_texture(image)
	get_tree().get_root().add_child(bullet)


func shoot():
	
	#Spread
	if (powerup[1]):
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(100, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(25, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(50, -500))
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(75, -500))
		create_bullet($Cannons/FrontCenter.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-100, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-75, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-50, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(-25, -500))
	else:
		create_bullet($Cannons/FrontRight.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontLeft.get_global_position(), Vector2(0, -500))
		create_bullet($Cannons/FrontCenter.get_global_position(), Vector2(0, -500))

func activate_power_up(powerup_number):
	$PowerUpAudio.play()
	powerup[powerup_number] = true
	emit_signal("powerup_change")
	
	if powerup_number == 0:
		activate_fire_rate_up()
	elif powerup_number == 1:
		activate_spread()
	elif powerup_number == 2:
		activate_shield()

func activate_fire_rate_up():
	$ShootTimer.wait_time = 0.1
	$FireRateUpDuration.stop()
	$FireRateUpDuration.start()
	
func deactivate_fire_rate_up():
	powerup[0] = false	
	$ShootTimer.wait_time = 0.25

func activate_spread():
	powerup[1] = true
	$SpreadDuration.stop()
	$SpreadDuration.start()

func deactivate_spread():
	powerup[1] = false
	
func activate_shield():
	powerup[2] = true
	hitable = false
	$Shield.visible = true
	$ShieldDuration.stop()
	$ShieldDuration.start()
	
func deactivate_shield():
	hitable = true
	powerup[2] = false
	$Shield.visible = false

#On Hit Something
func activate_ghost_mode():
	ghost_mode = true
	hitable = false
	if (dim_count % 2 == 0):
		$character.modulate = Color(255, 255, 255, 255)
	else:
		$character.modulate = Color(1,0.855469,0.855469,1)
	$GhostModeDuration.stop()
	$GhostModeDuration.start()
	
func deactivate_ghost_mode():
	hitable = true
	ghost_mode = false
	$character.modulate = Color(1,0.855469,0.855469,1)
	$GhostModeDuration.stop()	

#HERE
func create_explosion():
	var explosion = Destroyed.instance()
	explosion.position = get_global_position()
	get_tree().get_root().add_child(explosion)

func create_explosion_dead():
	var explosion = Explosion.instance()
	explosion.position = get_global_position()
	get_tree().get_root().add_child(explosion)

func _on_ShootTimer_timeout():
	if Global.dialog_on:
		return
	shoot()


func _on_Ship_dead():
	for i in range(Global.shadows.size()):
		var shadow = Global.shadows[i];
		if (shadow):
			get_node("../../Stage").remove_child(shadow)

func _on_SpreadDuration_timeout():
	deactivate_spread()
	emit_signal("powerup_change")

func _on_FireRateUpDuration_timeout():
	deactivate_fire_rate_up()
	emit_signal("powerup_change")
	
func _on_ShieldDuration_timeout():
	deactivate_shield()
	emit_signal("powerup_change")


func _on_GhostModeDuration_timeout():
	dim_count += 1
	if dim_count < max_dim_count:
		activate_ghost_mode()
	else:
		dim_count = 0
		deactivate_ghost_mode()

func _on_Remaining_powerup_duration_change(powerup, time_left):
	emit_signal("powerup_duration_change", powerup, time_left)
