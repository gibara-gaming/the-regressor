extends Timer

signal powerup_duration_change(powerup, time_left)

func _process(delta):
	if (self.time_left > 0):
		var time_left = str(int(self.time_left) % 60)
		emit_signal("powerup_duration_change", "firerateup", time_left)
