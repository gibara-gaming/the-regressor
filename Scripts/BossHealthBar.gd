extends Control


onready var boss_health_bar = $HealthBossBar


func _on_health_updated(health):
	boss_health_bar.value = health


func _on_max_health_updated(max_health):
	boss_health_bar.max_value = max_health


func _get_health():
	return boss_health_bar.value
	

func _get_max_health():
	return boss_health_bar.max_value
