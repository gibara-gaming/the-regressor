extends Node


export (PackedScene) var Enemy
export var enemy_kill_score = 10;
export (PackedScene) var Boss
export (PackedScene) var LastBoss
export var boss_kill_score = 100;
export var last_boss_kill_score = 200;
export (PackedScene) var Powerup
export var level = 1
export var transition_duration = 1.00
export var transition_type = 1 # TRANS_SINE

onready var tween_out = get_node("Tween")

var iteration = 0

var score
var kill_score = 0 setget set_kill_score
var shadowScene = preload("res://Scenes/Shadow.tscn");
var drop_rate_percentage = 0
var lastBoss = null

func _ready():
	Global.shadowScene = shadowScene;
	$Wave_1.connect('enemy_dead',self, '_on_Enemy_Dead')
	$"Wave_2-1".connect('enemy_dead',self, '_on_Enemy_Dead')
	$Wave_3.connect('enemy_dead',self, '_on_Enemy_Dead')

func _on_Enemy_Dead(mob):
	var powerup = Powerup.instance()
	powerup.position = mob.get_global_position()
	add_child(powerup)

	set_kill_score(kill_score + enemy_kill_score)

func _on_Boss_Dead():
	set_kill_score(kill_score + boss_kill_score)

func _on_Last_Boss_Dead():
	set_kill_score(kill_score + last_boss_kill_score)

func new_game():
	$HUD.run_dialog([
		'Underground Facility AI:\n99% completed ...',
		'Underground Facility AI:\n100% completed ...',
		'Underground Facility AI:\nSynchronizing ...',
		'Underground Facility AI:\nChecking surrounding environment ...',
		'Underground Facility AI:\nNo threat detected ...',
		'Underground Facility AI:\nSending subject 01-Ronne to surface ...',
		'Underground Facility AI:\n**Machine noise**',
		'Underground Facility AI:\nSubject 01-Ronne arrived at surface ...',
		'Underground Facility AI:\nReleasing subject 01-Ronne ...',
		'Underground Facility AI:\nSubject 01-Ronne released ...',
	], 1)

func _on_HUD_second_dialog():
	pass

func _on_HUD_next_iteration():
	start(1)
	$Ship.start($StartPosition.position)
	$HUD.start($Ship.armor, $Ship.skill, 1)

func flush_stage():
	$Wave_1.stop()
	$"Wave_2-1".stop()
	$Wave_3.stop()
	$WarningBoss.stop()
	$LastBossBgm.stop()
	$BossBgm.stop()
	$Wave3Bgm.stop()
	$WaveBgm.play()
	get_tree().call_group("enemy", "queue_free")
	get_tree().call_group("boss", "queue_free")
	get_tree().call_group("enemy_laser", "queue_free")
	get_tree().call_group("shadow", "queue_free")
	get_tree().call_group("powerup", "queue_free")
	get_tree().call_group("enemy_bullet", "queue_free")
	get_tree().call_group("player_bullet", "queue_free")

func game_over():
	flush_stage()
	$HUD.show_game_over()

func end_level():
	flush_stage()
	#$MobTimer.stop()
	$HUD.show_next_level()

func _boss_die():
	yield(get_tree().create_timer(6), "timeout")
	$HUD.run_dialog([
		'???:\nRonne ...',
		'???:\nYou have to be faster',
		'???:\nTheir main army is approaching earth',
		'???:\nThe tower barrier can\'t hold their main army power',
		'Ronne:\nHey, i still don\'t know who you are',
		'???:\n**Static noise**',
		'Ronne:\nWhat will happen once i arrived at the tower?',
		'???:\n**Static noise**',
		'Ronne:\nSigh, i will demand a detailed explanation once i get there',
	], 4)

func _last_boss_die():
	yield(get_tree().create_timer(6), "timeout")
	$HUD.run_dialog([
		'???:\nRonne ...',
		'Ronne:\nThat voice ...',
		'Ronne:\nSo it\'s you',
		'Ronne:\nI demand an explanation about all of it',
		'Raina:\nI\'m Raina, an AI left by Prof. Seiya to guide you',
		'Ronne:\nGuide me?',
		'Raina:\nYour lost your memory right?',
		'Raina:\nProf. Seiya already expect it',
		'Raina:\nNow, i\'ll recover your memory',
		'Ronne:\n**screaming in pain**',
		'Ronne:\nWhat is it? My head will explode',
		'Raina:\nI\'m recovering your memory',
		'Raina:\nYou will remember',
		'Raina:\nThe day when the war begin',
		'Raina:\nWhen aliens called Retra invading earth',
		'Raina:\nWhen humanity become one to fight them',
		'Raina:\nWhen humanity almost perish',
		'Raina:\nAnd when you participate in the experiment and become humanity last hope',
		'Ronne:\nArghh, now i remember',
		'Tower System:\n**Emergency siren**',
		'Raina:\nTheir main army already attacking the tower, we have no time',
		'Ronne:\nTell me what i have to do',
		'Raina:\nProf. Seiya and the other already gathered all the resource that we need to strengthen your power',
		'Raina:\nWith the resource saved in this tower, you can regress to five years ago',
		'Ronne:\nOk, i\'ll do that',
		'Ronne:\nI hope we will meet again',
		'Raina:\n**Nod**',
	], 6)
	var wisp = preload("res://Scenes/AfterLastBoss.tscn")
	var wisp_instance = wisp.instance()
	add_child(wisp_instance)

func _on_HUD_wave_3_start():
	fade_out($BossBgm)
	$Wave3Bgm.play()
	$Wave_3.run()

func fade_out(stream_player):
	# tween music volume down to 0
	tween_out.interpolate_property(stream_player, "volume_db", 0, -80, transition_duration, transition_type, Tween.EASE_IN, 0)
	tween_out.start()
	# when the tween ends, the music will be stopped

func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	object.stop()

func startBoss():

	$WaveBgm.stop()
	$WarningBoss.play()
	$BossBgmTimer.start()
	
	$HUD.run_dialog([
		'Rigurd:\nHmm, an earthling?',
		'Rigurd:\nI thought you already extinct',
		'Rigurd:\nSo, someone survived the extermination after all',
		'Ronne:\nWho are you?',
		'Rigurd:\nMe? You ignorant fool, you dont know me?',
		'Rigurd:\nI\'m, Rigurd, the right hand of Astaroth',
		'Ronne:\nWhy are you blocking my way?',
		'Rigurd:\nIt\'s simple, earthling should be dead',
		'Ronne:\nThen, can i ask you something before i die?',
		'Rigurd:\nHmm, i shouldn\'t give you any information',
		'Rigurd:\nBut, i will answer your question',
		'Rigurd:\nSince dead men tell no tales',
		'Ronne:\nBut i\'m a girl you know?',
		'Rigurd:\nWell, all earthling look the same to me',
		'Ronne:\nSo, do you know what is that tower?',
		'Rigurd:\nIt should be me who ask that question to you',
		'Rigurd:\nIt\'s built by earthling',
		'Rigurd:\nThat tower is still untouchable even after one years from the earthling extermination',
		'Rigurd:\nAnyone who enter that tower was disintegrated',
		'Ronne:\nHmm, that tower is really mysterious',
		'Ronne:\nThank you rigurd, i will give you a favor since you answered my question',
		'Ronne:\nI will kill you, so you won\'t be punished by Astaroth',
		'Rigurd:\nHaha, come and try me earthling',
	], 3)

	var mob = Boss.instance()
	add_child(mob)
	mob.connect("boss_dead", self, "_on_Boss_Dead")

func startLastBoss():
	$Wave3Bgm.stop()
	$WarningBoss.play()
	$LastBossBgmTimer.start()


	lastBoss = LastBoss.instance()
	add_child(lastBoss)
	lastBoss.connect("boss_dead", self, "_on_Boss_Dead")
	
func _on_LastBossBgmTimer_timeout():
	$LastBossBgmTimer.stop()
	$LastBossBgm.play()

func next_level():
	start(level+1)

func start(val):
	level = val
	score = level*10
	setPlayArea()
	iteration = iteration + 1
	$HUD.update_iteration(iteration)
	$HUD.update_skill($Ship.skill)
	$HUD.update_nuke($Ship.armor)
	$HUD.show_message("Get Ready\nHold shift\nto slow down")
	$StartTimer.start()
	setup()

func setup():
	$Ship.setup()

func setPlayArea():
	var min_x = $HUD/PlayArea.rect_position.x
	var max_x = $HUD/PlayArea.rect_position.x + (($HUD/PlayArea.rect_size.x * $HUD/PlayArea.rect_scale.x))
	var min_y = $HUD/PlayArea.rect_position.y
	var max_y = $HUD/PlayArea.rect_position.y + (($HUD/PlayArea.rect_size.y * $HUD/PlayArea.rect_scale.y))
	$Ship.set_play_area(min_x, max_x, min_y, max_y)

func _on_StartTimer_timeout():
	#$MobTimer.start()
	#$ScoreTimer.start()
	$HUD/PauseButton.show()
	$Wave_1.run()

func _on_Ship_hit():
	$HUD.update_nuke($Ship.armor)

func _on_Ship_skill():
	$HUD.update_skill($Ship.skill)

func set_kill_score(value):
	kill_score = value
	$HUD.update_kill_score(kill_score)

func _on_Wave_1_wave_done():
	yield(get_tree().create_timer(5), "timeout")
	$"Wave_2-1".run()

func _on_Wave_21_wave_done():
	yield(get_tree().create_timer(2), "timeout")
	startBoss()

func _on_Ship_powerup_change():
	$HUD.update_powerup($Ship.powerup)

func _on_Ship_powerup_duration_change(powerup, time_left):
	$HUD.update_powerup_duration(powerup, time_left)

func _on_BossBgmTimer_timeout():
	$BossBgmTimer.stop()
	$BossBgm.play()

func _on_HUD_quit_play():
	flush_stage()
	
	iteration = 0
	$HUD.update_iteration(iteration)
	
	set_kill_score(0)
	
	$Ship.skill = 2
	$HUD.update_skill($Ship.skill)
	
	$Ship.armor = 5
	$HUD.update_nuke($Ship.armor)

	$Ship/ShootTimer.stop()
	$Ship.hide()
	$Ship/CollisionShape2D.set_deferred("disabled", true)
	
	$Ship.start = false
	Global.shadows_movement = []
	Global.shadows = []
	Global.dialog_on = false

func _on_Wave_3_wave_done():
	yield(get_tree().create_timer(5), "timeout")
	$HUD.run_dialog([
		'Ronne:\nHello there, are you Astaroth?',
		'Astaroth:\nWhat do you want earthling?',
		'Astaroth:\nYou shouldn\'t leave your hideout if you want to live',
		'Astaroth:\nBut if you really want to die, i\'ll grant your wish.',
		'Ronne:\nWell, i dont want to die',
		'Ronne:\nI come here to deliver a news for you',
		'Ronne:\nThere is a blabbermouth on your side named Rigurd',
		'Ronne:\nI already killed him as a punishment',
		'Ronne:\nYou should thank me right?',
		'Astaroth:\nHow dare you killed my brother!',
		'Astaroth:\nI\'ll kill you so my brother can rest in peace',
		'Ronne:\nWell, I think it\'s better if you accompany him',
		'Ronne:\nHe can rest in peace if you are with him, right?',
	], 5)
	startLastBoss()
	
func _on_HUD_last_boss_start():
	lastBoss.startBattle()


func _on_HUD_end_part_start():
	get_node("HUD/WinScreen").visible = true
