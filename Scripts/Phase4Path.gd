extends Path2D

onready var follow = get_node("PathFollowP4")
onready var moveSpeed = 300

var nodeName = "Phase4Path"
var timer = 0

func _ready():
	set_process(false)

func _process(delta):
	var boss = self.get_parent().boss

	if (boss.state == "phase4"):
		timer += 1
		follow.set_offset(follow.get_offset() - moveSpeed * delta)

		if timer > 100:
			self.get_parent().end_phase()
			timer = 0
