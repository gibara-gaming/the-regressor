extends RichTextLabel

var dialog = []
var page = 0
var player_name = null;
var part_number = null

func run_dialog(current_dialog, current_part_number):
	part_number = current_part_number

	get_node("./../../DialogBox").visible = true
	
	if (part_number == 1):
		get_node("./../BlackScreen").visible = true

	Global.dialog_on = true;
	dialog = current_dialog;
	set_bbcode(dialog[page])
	
	name = dialog[page].split('\n')[0]
	set_visible_characters(name.length() + 2)
	
func stop_dialog():
	dialog = []
	page = 0
	Global.dialog_on = false;
	
	get_node("./../../DialogBox").visible = false
	
	if (part_number == 1):
		get_node("./../BlackScreen").visible = false
		run_dialog([
			'Ronne:\n**Blinking eyes**',
			'Ronne:\nWhere am i?',
			'Ronne:\n**Screaming in pain**',
			'Ronne:\nWho am i? I can\'t remember anything',
			'???:\nRonne ...',
			'Ronne:\nWho are you? Show yourself',
			'???:\nRonne ...',
			'Ronne:\nWho is Ronne?',
			'???:\nI\'m waiting for you Ronne ...',
			'Ronne:\nWhere are you?',
			'???:\nI\'m waiting for you at the tower ...',
			'Ronne:\nTower?',
			'???:\n**Static noise**',
			'Ronne:\nWhat do you mean?',
			'???:\n**Static noise**',
			'Ronne:\n**Looking around**',
			'Ronne:\nIs that the tower?',
			'Ronne:\nIt seems like I have to go to that tower',
		], 2)
		
	elif (part_number == 2):
		get_node("./../../../HUD").emit_signal('next_iteration')
		
	elif (part_number == 4):
		get_node("./../../../HUD").emit_signal('wave_3_start')
		
	elif (part_number == 5):
		get_node("./../../../HUD").emit_signal('last_boss_start')

	elif (part_number == 6):
		get_node("./../../../HUD").emit_signal('end_part_start')

func _process(_delta):
	if Global.dialog_on && Input.is_action_just_pressed('space'):
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size() - 1:
				page += 1
				set_bbcode(dialog[page])
				name = dialog[page].split('\n')[0]
				set_visible_characters(name.length() + 2)
				if name:
					set_visible_characters(name.length() + 2)
				else:
					set_visible_characters(0)
			else:
				stop_dialog()
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)
