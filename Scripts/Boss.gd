extends Area2D

const pos_entry = [250, -100]
const pos_default = [250, 150]
const pos_middle = [250, 350]

var health_bar = null

var state = 'entry'
var spin = 0

var spinning
var is_shooting = false
var is_spinning = false
var is_laser_delay = false
var is_health_bar_set = false

export var velocity = Vector2(0, 100)
export var level = 1
export var live = 500 setget set_live
export (PackedScene) var Bullet_enemy
export (PackedScene) var Laser_enemy
export (PackedScene) var Destroyed
signal hit
signal boss_dead


func _ready():
	init_health_bar()
	set_process(true)
	set_boss_position(pos_entry)
	$ShootTimer.wait_time = 0.2
	$ShootTimer.start()


func _process(delta):
	translate(velocity * delta)
	health_bar._on_health_updated(live+1)

	if (state == 'entry'):
		entry()

	if Global.dialog_on:
		return

	if (state == 'phase1'):
		phase1(delta)

		if (!is_health_bar_set):
			set_live(500+1)
			health_bar.show()
			is_health_bar_set = true

	if (state == 'phase2'):
		phase2()

	spawn_laser()


func init_health_bar():
	health_bar = self.get_parent().get_node("HealthBossBarControl")
	health_bar._on_max_health_updated(live+1)

func set_boss_position(pos):
	self.position.x = pos[0]
	self.position.y = pos[1]


func get_boss_position():
	return [int(self.position.x), int(self.position.y)]


func is_boss_pos_equal(pos):
	var spec = 3
	if (abs(int(self.position.x)-pos[0]) <= spec and abs(int(self.position.y)-pos[1]) <= spec):
		return true
	return false


func destroyed_anim():
	var ex = Destroyed.instance()
	ex.position = get_global_position()
	get_tree().get_root().add_child(ex)


func create_bullet(pos, direction):
	var bullet = Bullet_enemy.instance()
	bullet.position = pos
	bullet.rotation = direction
	bullet.velocity = bullet.velocity.rotated(direction)
	get_tree().get_root().add_child(bullet)


func shoot_laser(pos):
	var laser = Laser_enemy.instance()
	var GAP_FROM_CANNON = 100
	
	if pos == 'front':
		laser.position.y += GAP_FROM_CANNON
		self.get_node("Cannons/Front").add_child(laser)
		
	elif pos == 'back':
		laser.position.y -= GAP_FROM_CANNON
		self.get_node("Cannons/Back").add_child(laser)


func spawn_laser():
	if (state == "phase3" and !is_laser_delay):
		shoot_laser("front")
		shoot_laser("back")
		laser_delay(5)
		
		return
	
	if (is_laser_delay or _get_boss_health_ratio() >= 0.45):
		return
	
	shoot_laser('front')
	laser_delay(13)


func laser_delay(sec):
	is_laser_delay = true	
	
	var timer
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(sec)
	timer.autostart = true
	timer.connect("timeout", self, "_on_laser_delay_timeout")
	add_child(timer)


func _on_laser_delay_timeout():
	is_laser_delay = false


func shoot(pos):
	var direction = rotation
	if state == 'entry' or state == 'init':
		return
	if pos == 'left':
		create_bullet($Cannons/Left.get_global_position(), (direction + (PI/2)))
	elif pos == 'right':
		create_bullet($Cannons/Right.get_global_position(), (direction - (PI/2)))
	elif pos == 'front' and !(state in ['phase1', 'phase3']):
		create_bullet($Cannons/Front.get_global_position(), direction)
	elif pos == 'back' and (!(state in ['phase1', 'phase2', 'phase3']) or is_spinning):
		create_bullet($Cannons/Back.get_global_position(), direction + PI)
	elif pos == 'backright' and (!(state in ['phase2']) or is_spinning):
		create_bullet($Cannons/BackRight.get_global_position(), direction + PI)
	elif pos == 'backleft' and (!(state in ['phase2']) or is_spinning):
		create_bullet($Cannons/BackLeft.get_global_position(), direction + PI)
	elif pos == 'frontright' and is_spinning:
		create_bullet($Cannons/FrontRight.get_global_position(), direction)
	elif pos == 'frontleft' and is_spinning:
		create_bullet($Cannons/FrontLeft.get_global_position(), direction)


func _on_ShootTimer_timeout():
	if Global.dialog_on:
		return
	shoot('left')
	shoot('right')
	shoot('front')
	shoot('back')
	shoot('backright')
	shoot('backleft')
	shoot('frontright')
	shoot('frontleft')


func set_live(value):
	live = value
	if live <= 0:
		self.get_parent().get_parent().get_parent().get_parent()._boss_die()
		emit_signal("boss_dead")
		destroyed_anim()
		self.get_parent().get_parent().get_parent().queue_free()


func get_live():
	return live


func set_level(val):
	level = val


func _on_VisibilityNotifier2D_screen_exited():
	self.get_parent().get_parent().get_parent().queue_free()


func _on_Enemy_area_entered(area):
	if area.is_in_group("player"):
		area.armor -= 1

func _on_Init_timeout():
	state = 'phase1'


func spin_boss(delta, cnt):
	var spd_spin
	if (_get_boss_health_ratio() < 0.25):
		spd_spin = 1
	elif (_get_boss_health_ratio() < 0.5):
		spd_spin = 1
	else:
		spd_spin = 1
	
	rotation += spd_spin * delta
	rotation = fmod(rotation, 2 * PI)

	if (spinning == 2 and int(rotation) == 0):
		spinning = 1
		spin += 1
	elif (int(rotation) == 0):
		spinning = 1
	elif (spinning == 1 and int(rotation) == 6):
		spinning = 2


func entry():
	if (is_boss_pos_equal(pos_default)):
		state = 'init'
		init()


func init():
	velocity = Vector2(0, 0)
	$BossWave/Init.start()


func phase1(delta):
	if (_get_boss_health_ratio() < 0.25):
		velocity = Vector2(0, 200)
	elif (_get_boss_health_ratio() < 0.5):
		velocity = Vector2(0, 150)
	else:
		velocity = Vector2(0, 100)
	
	if (is_boss_pos_equal(pos_middle)):
		is_spinning = true
		velocity = Vector2(0, 0)
		spin_boss(delta, 1)
		
		var cnt_spin
		if (_get_boss_health_ratio() < 0.25):
			cnt_spin = 2
		elif (_get_boss_health_ratio() < 0.5):
			cnt_spin = 1
		else:
			cnt_spin = 1
		
		if (spin == cnt_spin):
			rotation = 0 # force
			spin = 0
			state = 'phase2'


func phase2():
	is_spinning = false
	
	if (_get_boss_health_ratio() < 0.25):
		velocity = Vector2(0, -300)
	elif (_get_boss_health_ratio() < 0.5):
		velocity = Vector2(0, -200)
	else:
		velocity = Vector2(0, -100)
	
	if (is_boss_pos_equal(pos_default)):
		velocity = Vector2(0, 0)
		state = 'phase3'

func decrement_live():
	live -= 1
	

func _get_boss_health_ratio():
	return health_bar._get_health()/health_bar._get_max_health()

# state:
# entry   -- masuk ke game
# init    -- diem
# phase1  -- ke tengah -> spin
# phase2  -- balik ke default
