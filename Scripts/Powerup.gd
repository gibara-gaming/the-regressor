extends Area2D

var powerup_list = [0,1,2]
var selected_powerup = null;

var powerup_1_image = preload("res://Assets/powerup/powerup_1.png")
var powerup_2_image = preload("res://Assets/powerup/powerup_2.png")
var powerup_3_image = preload("res://Assets/powerup/powerup_3.png")

onready var sprite = get_node("Sprite")

export var drop_rate_up_per_kill = 10
export var drop_rate_limit = 210

func _ready():
	randomize()

	get_parent().drop_rate_percentage += drop_rate_up_per_kill
	get_parent().drop_rate_percentage = get_parent().drop_rate_percentage % drop_rate_limit
	
	var chance = randi()%200+1
	if (chance > get_parent().drop_rate_percentage):
		queue_free()
		return

	get_parent().drop_rate_percentage = 0

	selected_powerup = powerup_list[randi() % powerup_list.size()]
	if selected_powerup == 0:
		sprite.set_texture(powerup_1_image)
	if selected_powerup == 1:
		sprite.set_texture(powerup_2_image)
	if selected_powerup == 2:
		sprite.set_texture(powerup_3_image)
	$Timer.start()

func _on_Powerup_area_entered(area):
	if area.is_in_group("player"):
		area.activate_power_up(selected_powerup)
		queue_free()


func _on_Timer_timeout():
	self.position.y += 1
	$Timer.start()
	


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
