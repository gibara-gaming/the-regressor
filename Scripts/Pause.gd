extends Control

signal resume_play
signal quit_confirmed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BackButton_pressed():
	$PauseMenu.hide()
	$Confirmation.show()

func _on_NoButton_pressed():
	$Confirmation.hide()
	$PauseMenu.show()

func _on_Resume_pressed():
	emit_signal("resume_play")

func _on_YesButton_pressed():
	_on_NoButton_pressed()
	emit_signal("quit_confirmed")
