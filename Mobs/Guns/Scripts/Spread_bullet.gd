extends "Bullet_type.gd"

export (float) var degree = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func shoot(pos=position, direction=rotation, secondary=false):
	extend_node.shoot(pos, direction)
	var dir_left = direction
	var dir_right = direction
	for i in (size-1):
		dir_left = dir_left - deg2rad(degree)
		dir_right = dir_right + deg2rad(degree)
		extend_node.shoot(pos, dir_left)
		extend_node.shoot(pos, dir_right)
