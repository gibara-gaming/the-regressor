extends "Shooting_pattern.gd"


export var start_direction = 0.0
export var rotation_speed = 1.0
var rotation_dir = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func shoot():
	extend_node.rotation_degrees = start_direction
	extend_node.shoot()
	
func _process(delta):
	extend_node.rotation += rotation_dir * rotation_speed * delta
