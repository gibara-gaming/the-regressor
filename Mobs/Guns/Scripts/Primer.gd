extends Node2D


export var extention : NodePath
export var stream = 1
#export var secondary = false
export (float) var wait = 0.25
onready var extend_node = get_node(extention)


func _ready():
	pass # Replace with function body.

func shoot(pos=get_global_position(), direction=get_global_rotation(), secondary=false):
	for i in stream:
		if !secondary:
			pos = get_global_position()
			direction = rotation
		extend_node.shoot(pos, direction, true)
		yield(get_tree().create_timer(wait), "timeout")
