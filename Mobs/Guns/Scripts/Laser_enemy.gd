extends Area2D


export var velocity = Vector2(0, 150)


func _ready():
	if !$AudioStreamPlayer.is_playing():
		$AudioStreamPlayer.play()
	set_process(true)


func _on_Bullet_enemy_area_entered(area):
	if area.is_in_group("player"):
		area.armor -= 1


func _on_Timer_timeout():
	queue_free()


func _on_VisibilityNotifier2D_screen_exited():
	pass
