extends Node2D


export (PackedScene) var bullet_enemy


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func shoot(pos=get_global_position(), direction=get_global_rotation(), secondary=false):
	var bullet = bullet_enemy.instance()
	bullet.position = pos
	bullet.rotation = direction
	bullet.velocity = bullet.velocity.rotated(direction)
	get_tree().get_root().add_child(bullet)
