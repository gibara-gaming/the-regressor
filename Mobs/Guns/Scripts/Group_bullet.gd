extends "Bullet_type.gd"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func shoot(pos=get_global_position(), direction=get_global_rotation(), secondary=false):
	extend_node.shoot(pos, direction)
	var pos_left = pos
	var pos_right = pos
	var trans_r = Vector2(-10, 10)
	trans_r = trans_r.rotated(direction)
	var trans_l = Vector2(10, 10)
	trans_l = trans_l.rotated(direction)
	
	for i in (size-1):
		pos_left.y = pos_left.y - trans_l.y
		pos_right.y = pos_right.y - trans_r.y
		pos_left.x = pos_left.x - trans_l.x
		pos_right.x = pos_right.x - trans_r.x
		extend_node.shoot(pos_left, direction)
		extend_node.shoot(pos_right, direction)
