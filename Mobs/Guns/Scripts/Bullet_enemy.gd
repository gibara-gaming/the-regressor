extends Area2D


export var velocity = Vector2(0, 150)


func _ready():
	set_process(true)


func _process(delta):
	translate(velocity * delta)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Bullet_enemy_area_entered(area):
	if area.is_in_group("player"):
		area.armor -= 1
		queue_free()

