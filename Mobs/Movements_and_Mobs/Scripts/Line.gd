extends "Mob_movement.gd"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()
	#mob.scale = Vector2(abs(mob.scale.x),abs(mob.scale.y))
	mob.set_global_rotation(0)
	if abs(abs(rotation_degrees)-90) <= 0.001:
		mob.scale = Vector2(abs(mob.scale.x),abs(mob.scale.y))
	else:
		mob.set_global_scale(Vector2(abs(mob.scale.x),abs(mob.scale.y)))
	#mob.set_global_scale(Vector2(abs(mob.scale.x),abs(mob.scale.y)))

func _on_Timer_timeout():
	if is_instance_valid(mob):
		mob.shoot()
	else:
		queue_free()
