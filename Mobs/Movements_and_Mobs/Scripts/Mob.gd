extends Area2D

export var enemy_type = "Enemy 1"
var enemy1_image = preload("res://Assets/enemy_1.png")
var enemy2_image = preload("res://Assets/enemy_2.png")
var enemy3_image = preload("res://Assets/enemy_3.png")
onready var sprite_node = get_node("Sprite")

export var live = 1
export var extention : NodePath
onready var gun = get_node(extention)
export (PackedScene) var Destroyed
signal enemy_dead

# Called when the node enters the scene tree for the first time.
func _ready():
	spriteConfig()
	
func spriteConfig():
	if enemy_type == "Enemy 1":
		sprite_node.set_texture(enemy1_image)
	if enemy_type == "Enemy 2":
		sprite_node.set_texture(enemy2_image)
	if enemy_type == "Enemy 3":
		sprite_node.set_texture(enemy3_image)

func shoot():
	gun.shoot()

func destroyed_anim():
	var ex = Destroyed.instance()
	ex.position = get_global_position()
	get_tree().get_root().add_child(ex)

func _on_Mob_area_entered(area):
	if area.is_in_group("player"):
		#Check if player shield on
		if area.hitable:
			area.armor -= 1
		
		if !area.ghost_mode: 
			destroyed_anim()
			emit_signal('enemy_dead')
			queue_free()

	if area.is_in_group("player_bullet") :
		if live <= 0:
			destroyed_anim()
			emit_signal('enemy_dead')
			queue_free()
			
