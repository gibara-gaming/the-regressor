extends "Mob_movement.gd"


export (float) var wait_before = 1
export (float) var wait_after = 1
export var wait_pos = [0.1654]
var stopped = false


func _ready():
	set_process(true)
	mob.set_global_rotation(0)
	mob.set_global_scale(Vector2(abs(mob.scale.x),abs(mob.scale.x)))

func _check():
	for i in wait_pos:
		if abs(i - $PathFollow2D.unit_offset) <= 0.004:
			stopped = true
			speed = 0
			yield(get_tree().create_timer(wait_before), "timeout")
			mob.shoot()
			yield(get_tree().create_timer(wait_after), "timeout")
			speed = base_speed
			stopped = false
			$PathFollow2D.offset += 8
		else:
			speed = base_speed
			
func _process(delta):
	if is_instance_valid(mob):
		if !stopped:
			_check()
		$PathFollow2D.offset += speed*delta
	else:
		queue_free()
	
	if $PathFollow2D.unit_offset >= 1:
		queue_free() 
