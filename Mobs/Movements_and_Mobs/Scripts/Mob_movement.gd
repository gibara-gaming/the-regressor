extends Path2D

export var base_speed = 50
onready var speed = base_speed
export var enemy : NodePath
onready var mob = get_node(enemy)

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(true)

func _process(delta):
	$PathFollow2D.offset += speed*delta
	
	if $PathFollow2D.unit_offset >= 1:
		queue_free() 
