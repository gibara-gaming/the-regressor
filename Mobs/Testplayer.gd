extends Area2D


export var base_speed = 400
export var extention : NodePath
export var armor = 3
var speed = base_speed
var screen_size
var invul = false

signal dead

onready var extend_node = get_node(extention)

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size
	$Timer.start()


func _process(delta):
	var velocity = Vector2()
	if Input.is_action_pressed("slow"):
		speed = base_speed/2
	else:
		speed = base_speed
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	
	if Input.is_action_just_pressed("shoot"):
		#var pos = $Cannons/Back.get_global_position()
		#$Spread_bullet.shoot(pos, rotation)
		extend_node.shoot()
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)


func _on_Timer_timeout():
	$Cannons/Primer.shoot()


func _on_Testplayer_area_entered(area):
	if (!invul) && (area.is_in_group("enemy") || area.is_in_group("boss") || area.is_in_group("enemy_bullet")):
		if armor <= 0:
			emit_signal("dead")
			$Timer.stop()
			hide()
			$CollisionShape2D.set_deferred("disabled", true)
		else:
			armor -= 1
			invul = true
			modulate.a = 0.5
			yield(get_tree().create_timer(2), "timeout")
			modulate.a = 1
			invul = false
