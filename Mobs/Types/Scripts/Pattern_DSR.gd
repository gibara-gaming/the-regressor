extends "Group.gd"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func spawn():
	$Timer.start()
	spawners[0].spawn()
	spawners[1].spawn()


func _on_Timer_timeout():
	spawners[2].spawn()
	spawners[3].spawn()
