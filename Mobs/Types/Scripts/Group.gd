extends Node2D


export (Array, NodePath) var spawns
var spawners = []

# Called when the node enters the scene tree for the first time.
func _ready():
	for o in spawns:
		spawners.append(get_node(o))
	yield(get_tree().create_timer(0.01), "timeout")
	#spawn()

func spawn():
	for o in spawners:
		o.spawn()

func stop():
	for _i in self.get_children():
		_i.stop()
