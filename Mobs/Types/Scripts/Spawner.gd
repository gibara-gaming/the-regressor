extends Position2D


export (PackedScene) var type
export var stack = 1
export var stream = 1
export var offset = [0,0]
export (float) var wait = 1
signal enemy_dead(mob)
var stop = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func spawn():
	stop = false
	for i in range(stream):
		for j in range(stack):
			if !stop:
				var mob = type.instance()
				mob.position.x = get_global_position().x + (j * offset[0])
				mob.position.y = get_global_position().y + (j * offset[1])
				mob.rotation = get_global_rotation()
				mob.scale.x = scale.x
				mob.scale.y = scale.y
				get_tree().get_root().add_child(mob)
				mob.get_node("PathFollow2D/Mob").connect('enemy_dead',self, '_on_Enemy_Dead', [ mob.get_node("PathFollow2D/Mob") ])
		yield(get_tree().create_timer(wait), "timeout")
		
func stop():
	stop = true

func _on_Enemy_Dead(dead_mob):
	#print(dead_mob.get_global_position())
	emit_signal("enemy_dead", dead_mob)
