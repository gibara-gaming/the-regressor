extends "Group.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func spawn():
	$Timer.start()
	spawners[0].spawn()
	spawners[1].spawn()
	spawners[4].spawn()


func _on_Timer_timeout():
	spawners[2].spawn()
	spawners[3].spawn()
