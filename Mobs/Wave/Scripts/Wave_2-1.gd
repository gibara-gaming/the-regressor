extends "res://Mobs/Types/Scripts/Group.gd"


export (Array, float) var timestamp
export var repeat = 0
var tick = -1.0
signal enemy_dead(mob)
signal wave_done

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in spawners:
		for j in i.spawners:
			j.connect('enemy_dead',self, '_on_Enemy_Dead')
	yield(get_tree().create_timer(0.01), "timeout")
	#run()
	
func run():
	tick = -1.0
	$Timer.start()

func _on_Timer_timeout():
	tick += 0.25
	if tick in timestamp:
		if tick == timestamp[-1]:
			if repeat > 0:
				repeat -= 1
				run()
			else:
				emit_signal("wave_done")
		else :
			spawners[timestamp.find(tick)].spawn()

func _on_Enemy_Dead(dead_mob):
	emit_signal("enemy_dead", dead_mob)
