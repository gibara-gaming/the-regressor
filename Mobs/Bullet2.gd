extends Area2D


export var velocity = Vector2(0, -500)
export (PackedScene) var Flare


func _ready():
	set_process(true)
	create_flare()


func _process(delta):
	translate(velocity * delta)


func create_flare():
	var flare = Flare.instance()
	flare.position = get_global_position()
	get_tree().get_root().add_child(flare)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Bullet_area_entered(area):
	if area.is_in_group("enemy"):
		create_flare()
		queue_free()

